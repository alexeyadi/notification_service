from django.db.models import CharField
from mailing.models import Client, Mailing, Message
from rest_framework.serializers import ModelSerializer
from rest_framework.validators import UniqueValidator


class MailingSerializer(ModelSerializer):
    """Сериалайзер для рассылки"""
    class Meta:
        model = Mailing
        fields = ('start_date',
                  'finish_date',
                  'text',
                  'mobile_code',
                  'tag'
                  )


class ClientSerializer(ModelSerializer):
    """Сериалайзер для клиента"""
    mobile = CharField(validators=[UniqueValidator(
        queryset=Client.objects.all())
    ])

    class Meta:
        model = Client
        fields = ('mobile',
                  'mobile_code',
                  'tag',
                  'time_zone'
                  )


class MessageSerializer(ModelSerializer):
    """Сериалайзер для сообщения"""
    class Meta:
        model = Message
        fields = ('pub_date',
                  'status',
                  'mailing',
                  'client'
                  )
        read_only_fields = ('status',)
