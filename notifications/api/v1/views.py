from django.shortcuts import get_object_or_404
from mailing.models import Client, Mailing, Message
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .serializers import ClientSerializer, MailingSerializer, MessageSerializer


class MailingViewSet(ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    @action(detail=True, methods=['get'])
    def get_stat(self, request, pk=None):
        """Статистика по одной рассылке"""
        queryset = Mailing.objects.all()
        mailing = get_object_or_404(queryset, pk=pk)
        serializer = MailingSerializer(mailing)
        return Response(serializer.data)

    @action(detail=False, methods=['get'])
    def get_all_stat(self, request):
        """Статистика по всем рассылкам"""
        queryset = Mailing.objects.all()
        serializer = MailingSerializer(queryset, many=True)
        return Response(serializer.data)


class ClientViewSet(ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MessageViewSet(ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
