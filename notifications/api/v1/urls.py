from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.routers import DefaultRouter

from .views import ClientViewSet, MailingViewSet, MessageViewSet

app_name = 'api_v1'

v1_router = DefaultRouter()
v1_router.register(r'clients', ClientViewSet, basename='clients')
v1_router.register(r'messages', MessageViewSet, basename='messages')
v1_router.register(r'mailing', MailingViewSet, basename='mailing')

schema_view = get_schema_view(
   openapi.Info(
      title='Notifications API',
      default_version='v1',
      description='Описание разработанного API для сервиса уведомлений',
      terms_of_service='https://www.google.com/policies/terms/',
      contact=openapi.Contact(email='contact@snippets.local'),
      license=openapi.License(name='BSD License'),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('', include(v1_router.urls)),
    path('docs/',
         schema_view.with_ui('swagger', cache_timeout=0),
         name='schema-swagger-ui'),
]
