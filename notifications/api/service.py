import logging
import os
import sys
import time

import requests
from dotenv import load_dotenv
from mailing.models import Client, Mailing, Message

load_dotenv()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    'Строка %(lineno)d - %(funcName)s - %(asctime)s - %(name)s'
    ' - %(levelname)s - %(message)s'
)
handler = logging.StreamHandler(stream=sys.stdout)
handler.setFormatter(formatter)
logger.addHandler(handler)


class MailingService():

    def __init__(self) -> None:
        self.endpoint = os.getenv('URL')
        self.token = os.getenv('JWT_TOKEN')
        self.header = {'Authorization': f'Bearer {self.token}',
                       'Content-Type': 'application/json'}
        self.retry_time = 10
        self.trying = 15

    def pick_up_mailing(id) -> list:
        """Собираем данные для рассылки."""
        mailing = Mailing.objects.get(pk=id)
        clients = Client.objects.filter(code=mailing.mobile_code, tag=mailing.tag)
        message_list = []
        for client in clients:
            messages = Message.objects.filter(
                client_id=client.id).select_related(
                'client', 'mailing')
            for message in messages:
                data = {
                    'id': message.id,
                    'mobile': client.mobile,
                    'text': mailing.text
                }
                message_list.append(data)
        return message_list

    def send_api_request(self, data):
        """Отправляем данные во внешний API."""
        logger.debug('Отправляем данные во внешний API...')
        for data in self.pick_up_mailing():
            response = requests.post(url=f"{self.enpoint}{data.pop('id')}",
                                     headers=self.auth_headers, json=data)
        return response

    def main(self):
        """Основная функция. Собирает логику воедино."""
        while True:
            count = 0
            data = self.pick_up_mailing()
            try:
                response = self.send_api_request(data)
                if response.status_code != 200 and count < self.trying:
                    time.sleep(self.retry_time)
                    count += 1
                else:
                    count = self.trying
                    logger.error('API недоступен')
                    break
            except Exception as error:
                message = f'Сбой в работе программы: {error}'
                logger.error(message)
            else:
                logger.info('Рассылка завершена удачно!')
