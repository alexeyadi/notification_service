# Generated by Django 2.2.19 on 2022-11-24 14:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mailing', '0004_auto_20221124_1904'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mailing',
            name='filter',
        ),
        migrations.AddField(
            model_name='mailing',
            name='mobile_code',
            field=models.CharField(blank=True, max_length=3, verbose_name='Код мобильного оператора'),
        ),
    ]
