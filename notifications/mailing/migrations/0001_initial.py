# Generated by Django 2.2.19 on 2022-11-24 13:44

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mobile', models.CharField(max_length=11, unique=True, validators=[django.core.validators.RegexValidator(message='Укажите номер телефона в формате 7ХХХХХХХХХХ', regex='^7\\d{10}$')], verbose_name='Номер телефона')),
                ('mobile_code', models.CharField(blank=True, max_length=3, verbose_name='Код мобильного оператора')),
                ('tag', models.CharField(blank=True, max_length=30, verbose_name='Тег')),
                ('time_zone', models.CharField(max_length=5, verbose_name='Часовой пояс')),
            ],
            options={
                'verbose_name': 'Клиент',
                'verbose_name_plural': 'Клиенты',
            },
        ),
        migrations.CreateModel(
            name='Mailing',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_date', models.DateTimeField(verbose_name='Запуск рассылки')),
                ('finish_date', models.DateTimeField(verbose_name='Окончание рассылки')),
                ('text', models.TextField(verbose_name='Текст сообщения')),
                ('mobile_code', models.CharField(blank=True, max_length=3, verbose_name='Код мобильного оператора')),
                ('tag', models.CharField(blank=True, max_length=30, verbose_name='Тег')),
            ],
            options={
                'verbose_name': 'Рассылка',
                'verbose_name_plural': 'Рассылки',
            },
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pub_date', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Дата и время отправки')),
                ('status', models.CharField(choices=[('Не было действий', 'Не было действий'), ('Отправлено', 'Отправлено'), ('Ждёт отправки', 'Ждёт отправки'), ('Ошибка', 'Ошибка')], default='Не было действий', max_length=20, verbose_name='Статус отправки')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='messages', to='mailing.Client', verbose_name='Клиент')),
                ('mailing', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='messages', to='mailing.Mailing', verbose_name='Рассылка')),
            ],
            options={
                'verbose_name': 'Сообщение',
                'verbose_name_plural': 'Сообщения',
                'ordering': ['-pub_date'],
            },
        ),
    ]
