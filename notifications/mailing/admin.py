from django.contrib import admin

from .models import Client, Mailing, Message


class MailingAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'text', 'start_date', 'finish_date')


class ClientAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'mobile', 'time_zone', 'tag')


class MessageAdmin(admin.ModelAdmin):
    list_display = ('mailing', 'client', 'status', 'pub_date')


admin.site.register(Mailing, MailingAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Message, MessageAdmin)
