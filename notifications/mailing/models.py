from django.core.validators import RegexValidator
from django.db import models
from django.utils import timezone


class Mailing(models.Model):
    start_date = models.DateTimeField(verbose_name='Запуск рассылки')
    finish_date = models.DateTimeField(verbose_name='Окончание рассылки')
    text = models.TextField(verbose_name='Текст сообщения')
    mobile_code = models.CharField(verbose_name='Код мобильного '
                                   'оператора',
                                   max_length=3,
                                   blank=True)
    tag = models.CharField(verbose_name='Тег',
                           max_length=30,
                           blank=True)

    @property
    def to_send_depend_on_timezone(self) -> bool:
        """Проверка попадания часового пояса клиента в рассылку"""
        tz_now = timezone.now()
        if self.start_date <= tz_now <= self.finish_date:
            return True
        else:
            return False

    @property
    def no_action(self):
        """Неотправленная рассылка"""
        return self.messages.filter(status='No action').count()

    @property
    def to_sent(self):
        """Отправленная рассылка"""
        return self.messages.filter(status='Sent').count()

    @property
    def to_sending(self):
        """Рассылка осуществляется в данный момент"""
        return self.messages.filter(status='Sending').count()

    @property
    def to_fail(self):
        """Ошибка в рассылке"""
        return self.messages.filter(status='Fail').count()

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'

    def __str__(self):
        return f'Рассылка {self.id}'


class Client(models.Model):
    mobile = models.CharField(verbose_name='Номер телефона',
                              validators=[RegexValidator(regex=r'^7\d{10}$',
                                          message='Укажите номер '
                                          'телефона в формате 7ХХХХХХХХХХ')],
                              max_length=11,
                              unique=True)
    mobile_code = models.CharField(verbose_name='Код мобильного '
                                   'оператора',
                                   max_length=3,
                                   blank=True)
    tag = models.CharField(verbose_name='Тег',
                           max_length=30,
                           blank=True)
    time_zone = models.CharField(verbose_name='Часовой пояс',
                                 max_length=5)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'

    def __str__(self):
        return f'Клиент {self.id}'


class Message(models.Model):
    NO_ACTION = 'Не было действий'
    SENT = 'Отправлено'
    SENDING = 'Ждёт отправки'
    FAIL = 'Ошибка'

    STATUSES = [
        (NO_ACTION, 'Не было действий'),
        (SENT, 'Отправлено'),
        (SENDING, 'Ждёт отправки'),
        (FAIL, 'Ошибка')
    ]

    pub_date = models.DateTimeField(
        verbose_name='Дата и время отправки',
        auto_now_add=True,
        db_index=True
    )
    status = models.CharField(verbose_name='Статус отправки',
                              choices=STATUSES,
                              default=NO_ACTION,
                              max_length=20,
                              )
    mailing = models.ForeignKey(Mailing,
                                verbose_name='Рассылка',
                                on_delete=models.CASCADE,
                                related_name='messages')
    client = models.ForeignKey(Client,
                               verbose_name='Клиент',
                               on_delete=models.CASCADE,
                               related_name='messages')

    class Meta:
        ordering = ['-pub_date']
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
